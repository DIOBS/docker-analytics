#!/usr/bin/env bash

/opt/spark/bin/spark-class org.apache.spark.deploy.master.Master --host ${SPARK_MASTER_HOST} --webui-port ${SPARK_WEBUI_PORT}
